#include "spi_simple.h"

void spi_master_init(void){
    /* Set MOSI and SCK output, all others input */
    DDRB = (1 << DDB0) | (1 << DDB1) | (1 << DDB2);
    PORTB |= (1 << PORTB0);
    /* Enable SPI, Master, set clock rate fck/16 */
    SPCR = (1 << SPE) | (1 << MSTR) | (1 << SPR0) | (1 << SPIE);
}

void spi_send(uint8_t cData) {
    PORTB &= ~(1 << PORTB0);

    /* Start transmission */
    SPDR = cData;
    /* Wait for transmission complete */
    // while(!(SPSR & (1 << SPIF)))
    // ;

    // PORTB |= (1 << PORTB0);
}

void spi_slave_init(void) {
    /* Set MISO output, all others input */
    DDRB = (1 << DDB3);
    /* Enable SPI */
    SPCR = (1 << SPE) | (1<<SPIE);
}

uint8_t spi_rece(void){
    /* Wait for reception complete */
    while (!(SPSR & (1 << SPIF)))
        ;
    /* Return data register */
    return SPDR;
}