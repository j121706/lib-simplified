#include "c4mlib/debug/src/debug.h"
#include "c4mlib/device/src/device.h"

// master : use PF0 as CS
// slave  : use SS 
void spi_master_init(void);

void spi_send(uint8_t cData);

void spi_slave_init(void);

uint8_t spi_rece(void);