#include "c4mlib\spibroadcast\src\spi_simple.h"
uint8_t master_rece;
int main() {
    C4M_DEVICE_set();

    uint8_t send_data = 0;
    DEBUG_INFO("--MASTER_START--\n");
    spi_master_init();
    
    sei();
    for (int i = 0; i < 5; i++)
    {
        _delay_ms(500);
        spi_send(send_data + i);
    }

    while (1)
    {
        _delay_ms(100);
        send_data = 0;
    }
    
    return 0;
}

ISR(SPI_STC_vect) {
    master_rece = SPDR;
    DEBUG_INFO("YA \n");
    DEBUG_INFO("rece = %d\n", master_rece);
    PORTB |= (1 << PORTB0);
}