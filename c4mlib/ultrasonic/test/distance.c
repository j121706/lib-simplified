#include "c4mlib/ultrasonic/src/sonic.h"


void timer_1_init(void);
volatile uint8_t flag = 0;
volatile uint32_t count = 0;
volatile double time = 0;

int main() {
    C4M_DEVICE_set();

    DDRF = 0b1;
    printf("--START--\n");
    timer_1_init();

    while (1)
    {
        DDRE = 0x80;
        PORTE = 0x80;
        _delay_us(2);
        PORTE = 0;
        __asm__("nop");
        __asm__("nop");
        DDRE = 0;

        _delay_us(700);

        while (1)
        {
            if ((PINE & 0x80) == 0x80)
            {
                flag = 1;
                sei();
                while (1)
                {
                    if ((PINE & 0x80) == 0)
                    {
                        cli();
                        flag = 0;
                        time = count * 6.2;
                        printf("distance: %3f cm\n", distance(time, 17) * 100);
                        count = 0;
                        break;
                    }
                }
                break;
            }
        }
        _delay_ms(100);
        
    }
   

    return 0;
}

void timer_1_init(void){
    TCCR1B |= (1 << WGM12) | (0 << CS12) | (0 << CS11) | (1 << CS10);
    OCR1A = 0x0002;
    TIMSK |= (1 << OCIE1A);
}

ISR(TIMER1_COMPA_vect){
    // PORTF ^= 0b1;
    if (flag == 1)
    {
        count ++;
    }
}