#include "sonic.h"

double distance(double time, int temperature){
    static double sonic = 0;
    sonic = 331.5 + (0.6 * temperature);
    return ((time / 1000000) * sonic) / 2;
}