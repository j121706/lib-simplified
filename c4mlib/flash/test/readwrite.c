/**
 * @file write.c
 * @author nacmvmclab (nacmvmclab@gmail.com)
 * @date 2021.04.26
 * @brief 
 * 
 */

#include "c4mlib/flash/src/flash.h"
#include "c4mlib/flash/src/spi.h"

#define WRITE_SIZE  10
#define READ_SIZE   20

uint8_t flash_addr[3] = {0x00, 0x00, 0x00};
uint8_t read_buffer[300] = {0};
uint8_t write_data[10] = {
    10, 9, 8, 7, 6, 
    1,  2, 3, 4, 5
};

int main(void) {
    C4M_DEVICE_set();
    system_spi_init();
    
    // _delay_ms(10);
    // printf("---Test start!!---\n");

    // int a = ext_flash_connect();
    // printf("ext_flash_connect: %d\n", a);

    // a = ext_flash_erase_64k(flash_addr);
    // printf("ext_flash_erase_sector: %d\n", a);

    // a = ext_flash_read(flash_addr, READ_SIZE, read_buffer);
    // for (uint8_t i = 0; i < 20; i++)
    //     printf("[before]read_buffer[%d]: %d\n", i, read_buffer[i]);
    
    // a = ext_flash_write_page(flash_addr, WRITE_SIZE, write_data);
    // printf("ext_flash_write_page: %d\n", a);

    // a = ext_flash_read(flash_addr, READ_SIZE, read_buffer);
    // for (uint8_t i = 0; i < 20; i++)
    //     printf("[after]read_buffer[%d]: %d\n", i, read_buffer[i]);
    
    // printf("---Test finish!!---\n");
    return 0;
}