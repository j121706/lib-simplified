#include "c4mlib/device/src/device.h"

#define SPI_PORT        PORTB
#define SPI_PORT_NUM    PB4
#define SPI_DDR         DDRB
#define SPI_DDR_NUM     DDB4

void system_spi_init(void);

void system_spi_deinit(void);

void spi_tran(uint8_t data);

uint8_t spi_recv(void);