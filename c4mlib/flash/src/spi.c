#include "spi.h"

void system_spi_init(void) {
    // SPI PIN output
    DDRB = (1 << DDB1) | (1 << DDB2);
    // SPI M/S CS PIN dir
    DDRB |= (1 << DDB0);
    // use B4 as CS pin
    SPI_DDR |= (1 << SPI_DDR_NUM);
    SPI_PORT |= (1 << SPI_PORT_NUM); 
    SPCR = (1 << SPE) | (1 << MSTR) | (1 << SPR0);
}

void system_spi_deinit(void) {
    DDRB = 0;
    SPCR = 0;
}

void spi_tran(uint8_t data) {
    SPDR = data;
    while(!(SPSR & (1<<SPIF)))
        ;
}

uint8_t spi_recv(void) {
    SPDR = 0xFF;
    while(!(SPSR & (1<<SPIF)))
        ;
    return SPDR;
}