#include "c4mlib/device/src/device.h"

#define OK    0
#define NOTOK 1

#define ADD_BYTES    3
#define READ_STATUS  0x05
#define WRITE_ENABLE 0x06
#define PAGE_PROGRAM 0x02
#define READ_DATA    0x03
#define READ_MANU    0x90
#define ERASE_64K    0xD8

uint8_t ext_flash_write_page(uint32_t *Addr, uint16_t Bytes, void *Data_p);

uint8_t ext_flash_read(uint32_t *Addr, uint16_t Bytes, void *Data_p);

void ext_flash_busy_wait(void);

uint8_t ext_flash_connect(void);

uint8_t ext_flash_erase_64k(uint32_t *Addr);

void cs_enable(void);

void cs_disable(void);

void spi_tran(uint8_t data);