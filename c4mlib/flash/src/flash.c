#include "flash.h"
#include "spi.h"

uint8_t ext_flash_write_page(uint32_t *Addr, uint16_t Bytes, void *Data_p) {
    uint8_t Command;
    // Write Enable
    cs_enable();
    Command = WRITE_ENABLE;
    spi_tran(Command);
    cs_disable();

    ext_flash_busy_wait();  // Wait until the process is done.

    cs_enable();
    // Command
    Command = PAGE_PROGRAM;
    spi_tran(Command);
    // Address 
    for (int i = (ADD_BYTES - 1); i >= 0; i--) {
        spi_tran(*((uint8_t *)Addr + i));
    }
    // Data
    for (uint8_t i = 0; i < Bytes; i++) {
        spi_tran(*((uint8_t *)Data_p + i));
    }
    cs_disable();
    ext_flash_busy_wait();  // Wait until the process is done.

    return OK;
}

uint8_t ext_flash_read(uint32_t *Addr, uint16_t Bytes, void *Data_p) {
    uint8_t Command;
    cs_enable();
    // Command
    Command = READ_DATA;
    spi_tran(Command);

    // Address 
    for (int i = (ADD_BYTES - 1); i >= 0; i--) {
        spi_tran(*((uint8_t *)Addr + i));
    }
    // Data
    for (uint16_t i = 0; i < Bytes; i++) {
        *((uint16_t *)Data_p + i) = spi_recv();
    }
    cs_disable();

    return OK;
}

void ext_flash_busy_wait(void) {
    static uint8_t status = 0;
    uint8_t Command;
    Command = READ_STATUS;

    cs_enable();
    spi_tran(Command);
    while (1) {
        status = spi_recv();

        if ((status & 0x01) == 0) {
            cs_disable();
            break;
        }
    }
}

uint8_t ext_flash_connect(void) {
    uint8_t Command;
    uint8_t recv[2] = {0};
    Command = READ_MANU;
    cs_enable();

    spi_tran(Command);
    spi_tran(0x00);
    spi_tran(0x00);
    spi_tran(0x00);
    spi_tran(0x00);
    recv[0] = spi_recv();
    recv[1] = spi_recv();

    cs_disable();

    if (recv[0] == 0xEF && recv[1] == 0x17) {
        return OK;    
    } else {
        return NOTOK;    
    }
}

uint8_t ext_flash_erase_64k(uint32_t *Addr) {
    uint8_t Command;
    // Write Enable
    Command = WRITE_ENABLE;
    cs_enable();
    spi_tran(Command);
    cs_disable();

    Command = ERASE_64K;
    cs_enable();
    spi_tran(Command);
    // Address 
    for (int i = (ADD_BYTES - 1); i >= 0; i--) {
        spi_tran(*((uint8_t *)Addr + i));
    }
    cs_disable();

    ext_flash_busy_wait();
    return OK;
}

void cs_enable(void) {
    SPI_PORT &= ~(1 << SPI_PORT_NUM);
}

void cs_disable(void) {
    SPI_PORT |= (1 << SPI_PORT_NUM);
}

