#include "c4mlib\bstest\src\bstest.h"

int main() {
    C4M_DEVICE_set();
    DEBUG_INFO("--START--\n");
    DDRB = 0b100;
    PORTB = 0b100;
    for (long i = 0; i < 2; i++)
        __asm__("nop");
    DEBUG_INFO("HIGH --> PINB = %d\n",(PINB & 0b10));
    PORTB = 0;
    for (long i = 0; i < 2; i++)
        __asm__("nop");
    DEBUG_INFO("LOW --> PINB = %d\n",(PINB & 0b10));

    DDRB = 0b10;
    PORTB = 0b10;
    for (long i = 0; i < 2; i++)
        __asm__("nop");
    DEBUG_INFO("HIGH --> PINB = %d\n",(PINB & 0b100));
    PORTB = 0;
    for (long i = 0; i < 2; i++)
        __asm__("nop");
    DEBUG_INFO("LOW --> PINB = %d\n",(PINB & 0b100));





    DDRB = 0b1000;
    PORTB = 0b1000;
    for (long i = 0; i < 2; i++)
        __asm__("nop");
    DEBUG_INFO("HIGH --> PINB = %d\n",(PINB & 0b1));
    PORTB = 0;
    for (long i = 0; i < 2; i++)
        __asm__("nop");
    DEBUG_INFO("LOW --> PINB = %d\n",(PINB & 0b1));

    DDRB = 0b1;
    PORTB = 0b1;
    for (long i = 0; i < 2; i++)
        __asm__("nop");
    DEBUG_INFO("HIGH --> PINB = %d\n",(PINB & 0b1000));
    PORTB = 0;
    for (long i = 0; i < 2; i++)
        __asm__("nop");
    DEBUG_INFO("LOW --> PINB = %d\n",(PINB & 0b1000));
    return 0;
}