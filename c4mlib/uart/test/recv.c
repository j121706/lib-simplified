#include "c4mlib/uart/src/uart.h"

int main(void){
    C4M_DEVICE_set();

    printf("---START---\n");
    uart1_init(115200);

    uint8_t t = 0;
    while (1)
    {
        t = uart1_receive();
        printf("t = %x\n", t);
    }
    return 0;
}