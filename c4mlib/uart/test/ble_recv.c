#include "string.h"
#include "c4mlib/uart/src/uart.h"
#include "c4mlib/uart/src/ble_at.h"



static char Buffer[300];
volatile static uint16_t Counter = 0;

uint8_t uart1_str_recv(char* Buffer);

int main(void){
    C4M_DEVICE_set();
    printf("---START---\n");
    _delay_ms(100);
    uart1_init(115200);

    DDRF = 0b1;
    timer_1_init();
    
    uint8_t length = 0;

    while (true)
    {

        sei();
        while (1)
        {
            length = uart1_str_recv(Buffer);

            if (length != 0){
                cli();
                printf("length = %d\n", length);
                printf("Recv = %s\n", Buffer);
                Ble_sendstring("RECV!");
                break;
            }
        }
        
    }
    
    return 0;
}

uint8_t uart1_str_recv(char* Buffer){
    uint16_t i = 0;
    strcmp(Buffer,"");
    while (true)
    {
        Counter = 0;
        
        while ( !(UCSR1A & (1<<RXC))){
            if (Counter >= 300){
                Counter = 0;
                return i;
            }
        }
        
        /* Get and return received data from buffer */
        *(Buffer + i) = UDR1;
        i++;
    }
}



ISR(TIMER1_COMPA_vect){
    PORTF ^= 0b1;
    Counter++;
}
