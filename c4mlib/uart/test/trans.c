#include "c4mlib/uart/src/uart.h"

int main(void){
    C4M_DEVICE_set();

    // uint8_t test[2] = {0x77, 0x77};

    printf("---START---\n");
    uart1_init(115200);

    // uint16_t t = 0;
    for (uint8_t i = 0; i < 100; i++)
    {
        uart1_transmit(i);
        _delay_ms(500);
    }
    
    return 0;
}