#include "string.h"
#include "c4mlib/uart/src/uart.h"
#include "c4mlib/uart/src/ble_at.h"



static uint8_t Buffer[300];
volatile static uint16_t Counter = 0;

uint8_t uart1_str_recv(uint8_t* Buffer);

int main(void){
    C4M_DEVICE_set();
    _delay_ms(100);
    uart1_init(115200);

    DDRF = 0b1;
    timer_1_init();
    
    uint8_t length = 0;

    // uint8_t Str[] = "AT+BAUD=?";
    uint8_t Str[100] = "";
    while (true)
    {
        printf("---START---\n");
        printf("Enter the AT Command:");
        scanf("%s", Str);

        Ble_sendstring(Str);
        
        printf("---END---\n");
    }
    
    
    return 0;
}

uint8_t uart1_str_recv(uint8_t* Buffer){
    static uint16_t i = 0;
    strcat(*Buffer,"");
    while (true)
    {
        Counter = 0;
        
        while ( !(UCSR1A & (1<<RXC))){
            if (Counter >= 300){
                Counter = 0;
                return i;
            }
        }
        
        /* Get and return received data from buffer */
        *(Buffer + i) = UDR1;
        i++;
    }
}



ISR(TIMER1_COMPA_vect){
    PORTF ^= 0b1;
    Counter++;
}
