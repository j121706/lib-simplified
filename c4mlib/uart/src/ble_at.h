/**
 * @file ble_at.h
 * @author nacmvmclab (nacmvmclab@gmail.com)
 * @date 2021.04.26
 * @brief 
 * 
 */

#include "c4mlib/device/src/device.h"
#include "string.h"
#include "uart.h"

// F_OCN = (F_CPU / 2 / Prescaler / (1 + OCRn))
// F_CPU = 11059200
// Prescaler = 8
// OCRn = 107
// --> F_OCN = 200 hz --> 0.1 ms (100us)

// Full At Command
// AT          
// AT_RX       
// AT_VERSION  
// AT_ROLE
// AT_NAME
// AT_ADDR
// AT_BAUD

void timer_1_init(void);

uint8_t Ble_sendstring(char* Str);
