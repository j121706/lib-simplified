/**
 * @file ble_at.c
 * @author nacmvmclab (nacmvmclab@gmail.com)
 * @date 2021.04.26
 * @brief 
 * 
 */

#include "ble_at.h"

uint8_t Ble_sendstring(char* Str){
    char Input[strlen(Str)];
    for (uint8_t i = 0; i < (strlen(Str)); i++)
    {
        Input[i] = *(Str + i);
    }
    for (size_t i = 0; i < sizeof(Input); i++)
        uart1_transmit(Input[i]);
    
    return 0;
}

void timer_1_init(void){
    TCCR1B |= (1 << WGM12) | (0 << CS12) | (1 << CS11) | (0 << CS10);
    OCR1A = 137;
    TIMSK |= (1 << OCIE1A);
}