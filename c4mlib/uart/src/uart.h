#include "c4mlib/debug/src/debug.h"
#include "c4mlib/device/src/device.h"

void uart1_init(uint32_t UART_BAUD);

void uart1_transmit(uint8_t data);

uint8_t uart1_receive(void);


void uart0_init(uint32_t UART_BAUD);

void uart0_transmit(uint8_t data);

uint8_t uart0_receive(void);