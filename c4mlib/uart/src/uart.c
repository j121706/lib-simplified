#include "c4mlib/uart/src/uart.h"

void uart1_init(uint32_t UART_BAUD){
    unsigned int baud;
    baud = F_CPU / 16 / UART_BAUD - 1;
    UBRR1H = (unsigned char)(baud >> 8);
    UBRR1L = (unsigned char)baud;

    UCSR1B |= (1 << RXEN1) | (1 << TXEN1);
    UCSR1C |= (3 << UCSZ10);
}

void uart1_transmit(uint8_t data){
    /* Wait for empty transmit buffer */
    while ( !( UCSR1A & (1<<UDRE1)) )
    ;
    /* Put data into buffer, sends the data */
    UDR1 = data;
}

uint8_t uart1_receive(void){
    /* Wait for data to be received */
    while ( !(UCSR1A & (1<<RXC)) )
    ;
    /* Get and return received data from buffer */
    return UDR1;
}


// void uart0_init(uint32_t UART_BAUD){
//     unsigned int baud;
//     baud = F_CPU / 16 / UART_BAUD - 1;
//     UBRR1H = (unsigned char)(baud >> 8);
//     UBRR1L = (unsigned char)baud;

//     UCSR0B |= (1 << RXEN0) | (1 << TXEN0);
//     UCSR0C |= (3 << UCSZ00);
// }

// void uart1_transmit(uint8_t data){

// }

// void uart1_receive(uint8_t data){

// }